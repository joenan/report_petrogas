from openerp import fields, models, api
import os
from os import path
from datetime import datetime, timedelta
from docx import Document
from docx.shared import Inches, Cm, Pt, Mm
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.table import WD_TABLE_ALIGNMENT
import xlwt
from xlsxwriter import *
from cStringIO import StringIO
import base64
from reportlab.pdfgen import canvas
from openerp.addons.report_petrogas.report.report_xlsx import ReportXlsx
from datetime import datetime, timedelta


satuan = ['', 'satu ', 'dua ', 'tiga ', 'empat ', 'lima ',
          'enam ', 'tujuh ', 'delapan ', 'sembilan ', 'sepuluh ',
          'sebelas ', 'dua belas ', 'tiga belas ', 'empat belas ',
          'lima belas ', 'enam belas ', 'tujuh belas ', 'delapan belas ', 'sembilan belas ']


def tulis(angka, kelipatan):
    terbilang = satuan[angka] + kelipatan
    return terbilang


def ratusPuluhSatuan(angka):
    ratus = angka // 100
    sisa_ratus = angka % 100
    puluh = sisa_ratus // 10
    satuan = angka % 10
    terbilang = ''

    if(ratus == 1):
        terbilang += tulis(0, "seratus ")
    elif(ratus != 1 and ratus >= 1):
        terbilang += tulis(ratus, "ratus ")
    if (sisa_ratus >= 20):
        terbilang += tulis(puluh, "puluh ")
        terbilang += tulis(satuan, "")
    elif(sisa_ratus < 20):
        terbilang += tulis(sisa_ratus, "")

    return terbilang


def angkaTerbilang(angka):
    milyar = angka // 1000000000
    sisa_milyar = angka % 1000000000
    juta = sisa_milyar // 1000000
    sisa_juta = sisa_milyar % 1000000
    ribu = sisa_juta // 1000
    sisa_ribu = sisa_juta % 1000
    terbilang = ''

    if(milyar >= 1):
        terbilang += ratusPuluhSatuan(milyar)
        terbilang += "milyar "
    if(juta >= 1):
        terbilang += ratusPuluhSatuan(juta)
        terbilang += "juta "
    if(ribu > 1):
        terbilang += ratusPuluhSatuan(ribu)
        terbilang += "ribu "
    elif(ribu == 1):
        terbilang += "seribu "
    terbilang += ratusPuluhSatuan(sisa_ribu)

    return terbilang


class WizardReportPetrogas(models.TransientModel):
    _name = 'report.petrogas'
    _description = 'Report Petrogas'

    action = fields.Selection(
        [('im_payment', 'IM Payment'), ('ppukp', 'PPUKP')])
    payment_type = fields.Many2one('im.payment.type')
    employee_id = fields.Many2one('hr.employee')
    batch_id = fields.Many2one('hr.payslip.run')
    nomor_ref = fields.Char('Nomor Referensi', required=True)
    request_date = fields.Date('Request Date', required=True)
    due_date = fields.Date('Due Date', required=True)
    amount = fields.Float('Amount')

    def formatdate(self, date):
        tgl = datetime.strptime(date, '%Y-%m-%d').strftime('%d %B %Y')
        return tgl

    def formatmonth(self, date):
        tgl = datetime.strptime(date, '%Y-%m-%d').strftime('%B %Y')
        return tgl

    def make_rows_bold(self, *rows):
        for row in rows:
            for cell in row.cells:
                for paragraph in cell.paragraphs:
                    for run in paragraph.runs:
                        run.font.bold = True

    @api.multi
    def print_doc(self):
        document = Document()

        def set_a4(section):
            section.page_height = Mm(297)
            section.page_width = Mm(210)
            section.left_margin = Mm(25.4)
            section.right_margin = Mm(25.4)
            section.top_margin = Mm(25.4)
            section.bottom_margin = Mm(25.4)
            section.header_distance = Mm(12.7)
            section.footer_distance = Mm(12.7)

        # document.add_picture(logo_path, width=Inches(2.0))
        subject = ''
        kepada = ''
        if self.action == 'im_payment':
            domain = [
                ('payslip_run_id', '=', self.batch_id.id),
                ('salary_rule_id', 'in', self.payment_type.rule_ids.ids),
            ]
            """--> START TYPE LOCATION <--"""
            if self.payment_type.location == 'both' and self.payment_type.split == True:
                location = ['Jakarta', 'Sorong']
                npwp = ['02.058.486.8-081.000', '02.058.486.8-951.001']
                # calculate amount Jkt
                domain_jkt = domain + \
                    [('employee_id.work_location', '=', 'jakarta')]
                slip_lines = self.env['hr.payslip.line'].search(domain_jkt)
                amount_jakarta = sum(
                    line.total for line in slip_lines) if slip_lines else 0.0
                amount_jakarta = '{0:,.0f}'.format(
                    amount_jakarta).replace(',', '.')
                # calculate amount Sorong
                domain_srg = domain + \
                    [('employee_id.work_location', '=', 'sorong')]
                slip_lines = self.env['hr.payslip.line'].search(domain_srg)
                amount_sorong = sum(
                    line.total for line in slip_lines) if slip_lines else 0.0
                amount_sorong = '{0:,.0f}'.format(
                    amount_sorong).replace(',', '.')
            elif self.payment_type.location == 'both' and not self.payment_type.split:
                location = 'Jakarta & Sorong'
            elif self.payment_type.location == 'jakarta':
                location = 'Jakarta'
                domain += [('employee_id.work_location', '=', 'jakarta')]
            elif self.payment_type.location == 'sorong':
                location = 'Sorong'
                domain += [('employee_id.work_location', '=', 'sorong')]
            """--> END TYPE LOCATION <--"""

            # calculate amount
            slip_lines = self.env['hr.payslip.line'].search(domain)
            amount = sum(
                line.total for line in slip_lines) if slip_lines else 0.0
            amount = '{0:,.0f}'.format(amount).replace(',', '.')

            logo_path = str(os.path.dirname(os.path.realpath(__file__)))[
                :-6] + '/static/src/img/petrogas.png'    # Path of the image file
            section = document.sections[0]
            header = section.header
            paragraph = header.paragraphs[0]
            # paragraph.text = "Left Text\tCenter Text\tRight Text"
            he = paragraph.add_run()
            he.add_picture(logo_path, width=Inches(2.0))
            paragraph.style = document.styles["Header"]

            """--> START TYPE PAYMENT NAME <--"""
            """--> sesuai name di payment type <--"""
            if self.payment_type.name == 'BPJS Kesehatan':
                subject = 'Permintaan Pembayaran BPJS Kesehatan Jakarta & Sorong \n \t\t\t     BPJS Kesehatan Jakarta and Sorong Payment Request'
                kepada1 = 'Dengan hormat, \n\nMohon ditindaklanjuti pembayaran BPJS Kesehatan periode ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' sebelum tanggal ' + \
                    str(self.formatdate(self.due_date)) + '.'
                kepada2 = 'Dear Sir, \n\nPlease assist to proceed payment for the following BPJS Kesehatan as of ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' not later than ' + \
                    str(self.formatdate(self.due_date)) + '.'
                terima1 = 'Terima kasih atas kerjasamanya.'
                terima2 = 'Thank you for your kind cooperation.'

                def table(self):
                    table = document.add_table(rows=1, cols=4)
                    table.style = 'TableGrid'
                    hdr_cells = table.rows[0].cells
                    hdr_cells[0].text = 'No'
                    hdr_cells[1].text = 'Remarks'
                    hdr_cells[2].text = 'Location'
                    hdr_cells[3].text = 'Amount'
                    row_cells = table.add_row().cells
                    row_cells[0].text = str(1)
                    row_cells[
                        1].text = 'Virtual Account (MANDIRI) : 8988890080000841'
                    row_cells[2].text = location
                    row_cells[3].text = ' Rp ' + \
                        str(amount) + ',-'
                    # set width
                    for cell in table.columns[0].cells:
                        cell.width = Cm(0.88)
                    for cell in table.columns[1].cells:
                        cell.width = Cm(7.25)
                    for cell in table.columns[2].cells:
                        cell.width = Cm(3.65)
                    for cell in table.columns[3].cells:
                        cell.width = Cm(4.26)
            elif self.payment_type.name == 'BPJS Tenaga Kerja':
                subject = 'Permintaan Pembayaran BPJS Tenaga Kerja Jakarta & Sorong \n\t\t\t     BPJS Tenaga Kerja Jakarta and Sorong Payment Request'
                kepada1 = 'Dengan hormat, \n \nMohon ditindak lanjuti pembayaran BPJS Tenaga Kerja periode ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' sebelum tanggal ' + \
                    str(self.formatdate(self.due_date)) + '.'
                kepada2 = 'Dear Sir,\n \nPlease assist to proceed payment for the following BPJS Tenaga Kerja as of ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' not later than ' + \
                    str(self.formatdate(self.due_date)) + '.'
                terima1 = 'Terima kasih atas kerjasamanya.'
                terima2 = 'Thank you for your kind cooperation.'

                def table(self):
                    table = document.add_table(rows=1, cols=4)
                    table.style = 'TableGrid'
                    hdr_cells = table.rows[0].cells
                    hdr_cells[0].text = 'No'
                    hdr_cells[1].text = 'Remarks'
                    hdr_cells[2].text = 'Location'
                    hdr_cells[3].text = 'Amount'
                    row_cells = table.add_row().cells
                    row_cells[0].text = str(1)
                    row_cells[
                        1].text = 'Fee Code / kode iuran: 1911 0290 4091'
                    row_cells[2].text = location
                    row_cells[3].text = ' Rp ' + \
                        str(amount) + ',-'
                    row_total = table.add_row().cells
                    row_total[0].text = str('Total')
                    row_total[3].text = ' Rp ' + str(amount) + ',-'
                    # set width
                    for cell in table.columns[0].cells:
                        cell.width = Cm(0.88)
                    for cell in table.columns[1].cells:
                        cell.width = Cm(7.25)
                    for cell in table.columns[2].cells:
                        cell.width = Cm(3.65)
                    for cell in table.columns[3].cells:
                        cell.width = Cm(4.26)
            elif self.payment_type.name == 'Deposit PPUKP':
                subject = 'PPUKP DPLK Mandiri ' + \
                    str(self.formatmonth(self.request_date))
                kepada1 = 'Dengan hormat, \n\nMohon agar ditindaklanjuti proses pembayaran PPUKP di bawah ini untuk periode ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' sebelum tanggal ' + \
                    str(self.formatdate(self.due_date)) + '.'
                kepada2 = 'Dear Sir,\n \nPlease assist to proceed payment for the following PPUKP for period of ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' not later than ' + \
                    str(self.formatdate(self.due_date)) + '.'
                terima1 = 'Terima kasih atas kerjasamanya.'
                terima2 = 'Thank you for your kind cooperation.'

                def table(self):
                    table = document.add_table(rows=1, cols=3)
                    table.style = 'TableGrid'
                    table.columns[0].width = Cm(1)
                    hdr_cells = table.rows[0].cells
                    hdr_cells[0].text = 'No'
                    hdr_cells[1].text = 'Remarks'
                    hdr_cells[2].text = 'Amount'
                    row_cells = table.add_row().cells
                    row_cells[0].text = str(1)
                    row_cells[
                        1].text = 'Virtual Account Mandiri : 8842720000000051'
                    ppukp_amount = '{0:,.0f}'.format(
                        self.batch_id.company_id.ppukp_amount).replace(',', '.')
                    row_cells[2].text = ' Rp ' + \
                        str(ppukp_amount) + ',-'
                    # set width
                    for cell in table.columns[0].cells:
                        cell.width = Cm(0.88)
                    for cell in table.columns[1].cells:
                        cell.width = Cm(9.25)
                    for cell in table.columns[2].cells:
                        cell.width = Cm(5.65)
            elif self.payment_type.name == 'DERMA BAKORUMKRIS':
                subject = 'Transfer Derma Tetap bulan ' + str(self.formatmonth(
                    self.request_date)) + '\n\t\t\t     Derma Tetap Transfer for ' + str(self.formatmonth(self.request_date))
                kepada1 = 'Dengan hormat, \n\nMohon ditindaklanjuti pembayaran Derma Tetap periode ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' sebelum tanggal ' + \
                    str(self.formatdate(self.due_date)) + '.'
                kepada2 = 'Dear Sir,\n \nPlease assist to proceed payment for the following Derma Tetap as of ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' not later than ' + \
                    str(self.formatdate(self.due_date)) + '.'
                terima1 = 'Terima kasih atas kerjasamanya.'
                terima2 = 'Thank you for your kind cooperation.'

                def table(self):
                    table = document.add_table(rows=1, cols=4)
                    table.style = 'TableGrid'
                    hdr_cells = table.rows[0].cells
                    hdr_cells[0].text = 'No'
                    hdr_cells[1].text = 'Bank Account'
                    hdr_cells[2].text = 'Remarks'
                    hdr_cells[3].text = 'Amount'
                    no = 0
                    no = no + 1
                    row_cells = table.add_row().cells
                    row_cells[0].text = str(no)
                    row_cells[
                        1].text = '1540003039462 (MANDIRI) Acc. Name : Fredrik Kwaar'
                    row_cells[2].text = 'Derma Tetap ' + \
                        str(self.formatmonth(self.request_date)) + \
                        ' Bakor Umkris'
                    row_cells[3].text = ' Rp ' + \
                        str(amount) + ',-'
                    row_total = table.add_row().cells
                    row_total[0].text = str('Total')
                    row_total[3].text = ' Rp ' + str(amount) + ',-'
                    self.make_rows_bold(table.rows[0], table.rows[2])
                    table.cell(2, 0).paragraphs[
                        0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
                    row_total[0].merge(row_total[1]).merge(row_total[2])
                    # set width
                    for cell in table.columns[0].cells:
                        cell.width = Cm(0.9)
                    for cell in table.columns[1].cells:
                        cell.width = Cm(5.65)
                    for cell in table.columns[2].cells:
                        cell.width = Cm(5.39)
                    for cell in table.columns[3].cells:
                        cell.width = Cm(4.1)
            elif self.payment_type.name == 'Donation BDI Jakarta':
                subject = 'Transfer Sodaqoh bulan ' + str(self.formatmonth(
                    self.request_date)) + ' BDI Jakarta\n\t\t\t     Sodaqoh Transfer for ' + str(self.formatmonth(self.request_date)) + ' BDI Jakarta'
                kepada1 = 'Dengan hormat, \n\n\nMohon ditindaklanjuti pembayaran Sodaqoh periode ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' sebelum tanggal ' + \
                    str(self.formatdate(self.due_date)) + '.'
                kepada2 = 'Dear Sir,\n \nPlease assist to proceed payment for the following Sodaqoh as of  ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' not later than ' + \
                    str(self.formatdate(self.due_date)) + '.'
                terima1 = 'Terima kasih atas kerjasamanya.'
                terima2 = 'Thank you for your kind cooperation.'

                def table(self):
                    table = document.add_table(rows=1, cols=4)
                    table.style = 'TableGrid'
                    hdr_cells = table.rows[0].cells
                    hdr_cells[0].text = 'No'
                    hdr_cells[1].text = 'Bank Account'
                    hdr_cells[2].text = 'Remarks'
                    hdr_cells[3].text = 'Amount'
                    row_cells = table.add_row().cells
                    row_cells[0].text = str(1)
                    row_cells[
                        1].text = '3010174586 (Bank Muamalat) Acc. Name : Mimpi Retnowati or Novikasari'
                    row_cells[
                        2].text = 'Sodaqoh ' + str(self.formatmonth(self.request_date)) + ' for BDI JKT'
                    row_cells[3].text = ' Rp ' + \
                        str(amount) + ',-'
                    row_total = table.add_row().cells
                    row_total[0].text = str('Total')
                    row_total[3].text = ' Rp ' + str(amount) + ',-'
                    self.make_rows_bold(table.rows[0], table.rows[2])
                    table.cell(2, 0).paragraphs[
                        0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
                    row_total[0].merge(row_total[1]).merge(row_total[2])
                    # set width
                    for cell in table.columns[0].cells:
                        cell.width = Cm(0.9)
                    for cell in table.columns[1].cells:
                        cell.width = Cm(5.65)
                    for cell in table.columns[2].cells:
                        cell.width = Cm(5.39)
                    for cell in table.columns[3].cells:
                        cell.width = Cm(4.1)
            elif self.payment_type.name == 'Donation BDI KMT':
                subject = 'Transfer Sodaqoh ' + str(self.formatmonth(self.request_date)) + \
                    ' KMT\n\t\t\t     Sodaqoh Transfer ' + \
                    str(self.formatmonth(self.request_date)) + ' KMT'
                kepada1 = 'Dengan hormat, \n\nMohon ditindaklanjuti pembayaran Sodaqoh periode ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' sebelum tanggal ' + \
                    str(self.formatdate(self.due_date)) + '.'
                kepada2 = 'Dear Sir,\n \nPlease assist to proceed payment for the following Sodaqoh as of  ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' not later than ' + \
                    str(self.formatdate(self.due_date)) + '.'
                terima1 = 'Terima kasih atas kerjasamanya.'
                terima2 = 'Thank you for your kind cooperation.'

                def table(self):
                    table = document.add_table(rows=1, cols=4)
                    table.style = 'TableGrid'
                    hdr_cells = table.rows[0].cells
                    hdr_cells[0].text = 'No'
                    hdr_cells[1].text = 'Bank Account'
                    hdr_cells[2].text = 'Remarks'
                    hdr_cells[3].text = 'Amount'
                    row_cells = table.add_row().cells
                    row_cells[0].text = str(1)
                    row_cells[
                        1].text = '8510024982 (Bank Muamalat) Acc. Name : Achsan Abdullah'
                    row_cells[
                        2].text = 'Sodaqoh ' + str(self.formatmonth(self.request_date)) + ' for BDI KMT'
                    row_cells[3].text = ' Rp ' + \
                        str(amount) + ',-'
                    row_total = table.add_row().cells
                    row_total[0].text = str('Total')
                    row_total[3].text = ' Rp ' + str(amount) + ',-'
                    self.make_rows_bold(table.rows[0], table.rows[2])
                    table.cell(2, 0).paragraphs[
                        0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
                    row_total[0].merge(row_total[1]).merge(row_total[2])
                    # set width
                    for cell in table.columns[0].cells:
                        cell.width = Cm(0.9)
                    for cell in table.columns[1].cells:
                        cell.width = Cm(5.65)
                    for cell in table.columns[2].cells:
                        cell.width = Cm(5.39)
                    for cell in table.columns[3].cells:
                        cell.width = Cm(4.1)
            elif self.payment_type.name == 'PPh21':
                subject = 'SPT Masa Payment/ Employee PPh 21 for ' + \
                    str(self.formatmonth(self.request_date))
                kepada1 = 'Dengan hormat, \n\nMohon agar ditindaklanjuti proses pembayaran PPh21 di bawah ini untuk periode ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' sebelum tanggal ' + \
                    str(self.formatdate(self.due_date)) + '.'
                kepada2 = 'Dear Sir,\n \nPlease assist to proceed payment for the following income tax as of ' + \
                    str(self.formatmonth(self.request_date)) + \
                    ' not later than ' + \
                    str(self.formatdate(self.due_date)) + '.'
                terima1 = 'Terima kasih atas kerjasamanya.'
                terima2 = 'Thank you for your kind cooperation.'

                def table(self):
                    table = document.add_table(rows=1, cols=4)
                    table.style = 'TableGrid'
                    hdr_cells = table.rows[0].cells
                    hdr_cells[0].text = 'No'
                    hdr_cells[1].text = 'NPWP'
                    hdr_cells[2].text = 'Location'
                    hdr_cells[3].text = 'Amount'
                    no = 0
                    total = 0
                    for batch in location:
                        no = no + 1
                        row_cells = table.add_row().cells
                        row_cells[0].text = str(no)
                        row_cells[1].text = npwp[no - 1]
                        row_cells[2].text = location[no - 1]
                        row_cells[3].text = ' Rp ' + \
                            str(amount_jakarta) + \
                            ',-' if batch == 'Jakarta' else ' Rp ' + \
                            str(amount_sorong) + ',-'
                    row_total = table.add_row().cells
                    row_total[0].text = str('Total')
                    row_total[3].text = ' Rp ' + str(amount) + ',-'
                    self.make_rows_bold(table.rows[0], table.rows[
                                        len(table.rows) - 1])
                    table.cell(len(table.rows) - 1, 0).paragraphs[
                        0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
                    row_total[0].merge(row_total[1]).merge(row_total[2])
                    # set width
                    for cell in table.columns[0].cells:
                        cell.width = Cm(0.9)
                    for cell in table.columns[1].cells:
                        cell.width = Cm(5.65)
                    for cell in table.columns[2].cells:
                        cell.width = Cm(5.39)
                    for cell in table.columns[3].cells:
                        cell.width = Cm(4.1)

            elif self.payment_type.name == 'PPIP':
                subject = 'Permintaan Pembayaran Iuran Program Pensiun Iuran Pasti \n\t\t\tProgram Pensiun Iuran Pasti Payment Request.'
                kepada1 = 'Dengan hormat, \n\nMohon ditindaklanjuti pembayaran Program Pensiun Iuran Pasti (PPIP)  periode ' + str(
                    self.formatmonth(self.request_date)) + ' sebelum tanggal ' + str(self.formatdate(self.due_date)) + '.'
                kepada2 = 'Dear Sir,\n \nPlease assist to proceed payment for the following Program Pensiun Iuran Pasti (PPIP) for period of ' + str(
                    self.formatmonth(self.request_date)) + ' before ' + str(self.formatdate(self.due_date)) + '.'
                terima1 = 'Terima kasih atas kerjasamanya.'
                terima2 = 'Thank you for your kind cooperation.'

                def table(self):
                    table = document.add_table(rows=1, cols=4)
                    table.style = 'TableGrid'
                    table.columns[0].width = Cm(1)
                    table.columns[1].width = Cm(8)
                    hdr_cells = table.rows[0].cells
                    hdr_cells[0].text = 'No'
                    hdr_cells[1].text = 'Bank Account'
                    hdr_cells[2].text = 'Location'
                    hdr_cells[3].text = 'Amount'
                    no = 0
                    total = 0
                    if self.payment_type.split == True and self.payment_type.location == 'both':
                        for batch in location:
                            no = no + 1
                            total = total + int(self.amount)
                            row_cells = table.add_row().cells
                            row_cells[0].text = str(no)
                            row_cells[
                                1].text = 'Virtual Account (Mandiri)  : 8842710000000140'
                            row_cells[2].text = location[no - 1]
                            row_cells[3].text = ' Rp ' + \
                                str(self.amount) + ',-'
                        row_total = table.add_row().cells
                        row_total[0].text = str('Total')
                        row_total[3].text = ' Rp ' + str(total) + ',-'

                    else:
                        for batch in range(1):
                            no = no + 1
                            total = total + int(self.amount)
                            row_cells = table.add_row().cells
                            row_cells[0].text = str(no)
                            row_cells[
                                1].text = 'Virtual Account (Mandiri)  : 8842710000000140'
                            row_cells[2].text = location
                            row_cells[3].text = ' Rp ' + \
                                str(self.amount) + ',-'
                        row_total = table.add_row().cells
                        row_total[0].text = str('Total')
                        row_total[3].text = ' Rp ' + str(total) + ',-'
            """--> END TYPE PAYMENT NAME <--"""

            """--> START CALL DOC <--"""
            head = document.add_paragraph()
            head.add_run('INTERNAL MEMORANDUM').bold = True
            document.add_paragraph('Kepada \t| To \t   : Finance Manager \nDari \t \t| From \t   : HR Manager \nNo. \t\t| Ref \t   : ' + str(
                self.nomor_ref) + ' \nTanggal \t| Date \t   : ' + str(self.formatdate(self.request_date)) + ' \nPerihal \t| Subject : ' + subject + ' \n')
            table_kepada = document.add_table(rows=1, cols=2)
            hdr_cells_kepada = table_kepada.rows[0].cells
            hdr_cells_kepada[0].text = kepada1
            hdr_cells_kepada[1].text = kepada2
            table(self)
            table_terima = document.add_table(rows=2, cols=2)
            hdr_cells_terima = table_terima.rows[1].cells
            hdr_cells_terima[0].text = terima1
            hdr_cells_terima[1].text = terima2
            approval1 = document.add_paragraph('Menyetujui, / 1st Approval,')
            approval1.add_run(
                '\n\n\n\nAde Damanhuri \nHuman Resources Manager\n').bold = True
            approval1.add_run(datetime.now().strftime('Date:    /%m/%Y'))
            approval2 = document.add_paragraph(
                'Menyetujui,             / 2nd Approved,')
            approval2.add_run(
                '\n\n\n\nSyafri Syafar \nGeneral Manager / Country Manager\n').bold = True
            approval2.add_run(datetime.now().strftime('Date:    /%m/%Y'))
            """--> END CALL DOC <--"""
            # document.add_page_break()
            file_with_path = str(os.path.dirname(os.path.realpath(__file__)))[
                :-6] + '/static/doc/' + 'IM Payment Request - ' + str(self.payment_type.name) + '.docx'
            if path.exists(file_with_path):
                os.remove(file_with_path)
            document.save(file_with_path)
            return {
                'type': 'ir.actions.act_url',
                'url': 'report_petrogas/static/doc/' + 'IM Payment Request - ' + str(self.payment_type.name) + '.docx',
                'target': 'self',
            }

        else:
            # calculate amount
            amount = self.amount
            amount = '{0:,.0f}'.format(amount).replace(',', '.')

            logo_path_left = str(os.path.dirname(os.path.realpath(__file__)))[
                :-6] + '/static/src/img/skkmigas.png'    # Path of the image file
            logo_path_right = str(os.path.dirname(os.path.realpath(__file__)))[
                :-6] + '/static/src/img/petrogas.png'    # Path of the image file
            section = document.sections[0]
            set_a4(section)
            section.header_distance = Mm(6)
            header = section.header
            header_table = header.add_table(1, 3, Inches(6))
            htab_cells = header_table.rows[0].cells
            htp = htab_cells[0].paragraphs[0]
            kh = htp.add_run()
            kh.add_picture(logo_path_left, width=Inches(1.3))
            htp_text = htab_cells[1].paragraphs[0]
            htp_text.add_run('Petrogas (Basin) Ltd\n').bold = True
            htp_text.add_run(
                '53rd Floor, Sahid Sudirman Center\n' +
                'Jalan Jend. Sudirman No. 86\n' +
                'Jakarta 10220 - Indonesia\n' +
                'Telp. +62 21 29880628\n' +
                'Fax. +62 21 29880623')
            htp = htab_cells[2].paragraphs[0]
            kh = htp.add_run()
            kh.add_picture(logo_path_right, width=Inches(1.8))
            paragraph_format = document.styles['Header'].paragraph_format
            paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
            style = document.styles['Header']
            font = style.font
            font.name = 'Arial'
            font.size = Pt(7.5)
            htp_text.style = document.styles['Header']
            # set width
            for cell in header_table.columns[0].cells:
                cell.width = Inches(1.8)
            for cell in header_table.columns[1].cells:
                cell.width = Inches(2.7)
            for cell in header_table.columns[2].cells:
                cell.width = Inches(2)

            document.add_paragraph()
            headrow = document.add_table(rows=1, cols=2)
            paragraph_format = document.styles['Normal'].paragraph_format
            paragraph_format.line_spacing = 1
            paragraph_format.space_after = Pt(0)
            style = document.styles['Normal']
            font = style.font
            font.name = 'Arial'
            # headrow.style = 'TableGrid'
            hdr_cells_head = headrow.rows[0].cells
            hdr_cells_head[0].text = 'Ref. No.\t:  ' + str(self.nomor_ref)
            hdr_cells_head[1].text = 'Jakarta , ' + \
                str(self.formatdate(self.request_date))
            hdr_cells_head[1].paragraphs[
                0].alignment = WD_ALIGN_PARAGRAPH.RIGHT

            document.add_paragraph()
            kep = document.add_paragraph('Kepada')
            kep.add_run(
                '\nDANA PENSIUN LEMBAGA KEUANGAN\nPT BANK MANDIRI (PERSERO) Tbk.').bold = True
            kep.add_run('\nPlaza Mandiri Lantai 7')
            kep.add_run('\nJl. Jendral Gatot Subroto Kav. 36 - 38')
            kep.add_run('\nJakarta 12190')

            document.add_paragraph()
            headperihal = document.add_table(rows=1, cols=3)
            # headperihal.style = 'TableGrid'
            hdr_cells_perihal = headperihal.rows[0].cells
            hdr_cells_perihal[0].text = 'Perihal'
            hdr_cells_perihal[1].text = ':'
            hdr_cells_perihal[
                2].text = 'Permintaan Pembayaran Manfaat Dana PPUKP Untuk Pegawai Kami\n'
            hdr_cells_perihal[2].paragraphs[0].runs[0].font.bold = True
            # set width
            for cell in headperihal.columns[0].cells:
                cell.width = Cm(2)
            for cell in headperihal.columns[1].cells:
                cell.width = Cm(0.5)
            for cell in headperihal.columns[2].cells:
                cell.width = Cm(14)

            document.add_paragraph()
            hormat = document.add_paragraph('Dengan hormat,')

            document.add_paragraph()
            document.add_paragraph()
            opening1 = document.add_paragraph(
                'Menunjuk Perjanjian Kerjasama Pengelolaan Dana Program Pensiun untuk Kompensasi Pesangon (PPUKP) antara RH Petrogas (Basin) Ltd. dengan Dana Pensiun Lembaga Keuangan PT Bank Mandiri (Persero) Tbk. Nomor : CBG.DPLK/PKS-PPUKP.014/2016 tanggal 15 Maret 2016.')
            opening1.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
            document.add_paragraph()
            opening2 = document.add_paragraph(
                'Bersama ini kami sampaikan bahwa Karyawan Kami yang tersebut dibawah ini telah berakhir hubungan kerjanya, yaitu :')
            opening2.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

            keterangan = document.add_table(rows=1, cols=3)
            row_keterangan_nama = keterangan.rows[0]
            keterangan_nama = row_keterangan_nama.cells
            keterangan_nama[0].text = '  Nama'
            keterangan_nama[1].text = ':'
            keterangan_nama[2].text = str(self.employee_id.name)

            row_keterangan_NIP = keterangan.add_row()
            keterangan_NIP = row_keterangan_NIP.cells
            keterangan_NIP[0].text = '  NIP'
            keterangan_NIP[1].text = ':'
            keterangan_NIP[2].text = str(self.employee_id.identification_id)

            row_keterangan_tanggal_lahir = keterangan.add_row()
            keterangan_tanggal_lahir = row_keterangan_tanggal_lahir.cells
            keterangan_tanggal_lahir[0].text = '  Tanggal Lahir'
            keterangan_tanggal_lahir[1].text = ':'
            keterangan_tanggal_lahir[2].text = str(self.employee_id.birthday)

            row_keterangan_KTP = keterangan.add_row()
            keterangan_KTP = row_keterangan_KTP.cells
            keterangan_KTP[0].text = '  Nomor KTP'
            keterangan_KTP[1].text = ':'
            # keterangan_KTP[2].text = str('1231275371253')
            keterangan_KTP[2].text = str(self.employee_id.ktp)

            row_keterangan_NPWP = keterangan.add_row()
            keterangan_NPWP = row_keterangan_NPWP.cells
            keterangan_NPWP[0].text = '  NPWP'
            keterangan_NPWP[1].text = ':'
            # keterangan_NPWP[2].text = str('127368127368')
            keterangan_NPWP[2].text = str(self.employee_id.npwp)

            row_keterangan_dana = keterangan.add_row()
            keterangan_dana = row_keterangan_dana.cells
            keterangan_dana[0].text = '  Jumlah Dana PPUKP'
            keterangan_dana[1].text = ':'
            keterangan_dana[2].text = 'Rp ' + str(amount) + ' (' + str(
                angkaTerbilang(int(self.amount))) + 'rupiah) dan belum dipotong pajak'

            row_keterangan_rek = keterangan.add_row()
            keterangan_rek = row_keterangan_rek.cells
            keterangan_rek[0].text = '  Nomor Rekening'
            keterangan_rek[1].text = ':'
            keterangan_rek[2].text = str(
                self.employee_id.bank_account_id.acc_number)

            row_keterangan_rek_pem = keterangan.add_row()
            keterangan_rek_pem = row_keterangan_rek_pem.cells
            keterangan_rek_pem[0].text = '  Nama Pemilik Rekening'
            keterangan_rek_pem[1].text = ':'
            keterangan_rek_pem[2].text = str(
                self.employee_id.bank_account_id and
                self.employee_id.bank_account_id.owner_name or
                self.employee_id.name)

            row_keterangan_rek_pen = keterangan.add_row()
            keterangan_rek_pen = row_keterangan_rek_pen.cells
            keterangan_rek_pen[0].text = '  Nama Bank Penerima'
            keterangan_rek_pen[1].text = ':'
            keterangan_rek_pen[2].text = str(
                self.employee_id.bank_account_id.bank_name)
            # set width
            for cell in keterangan.columns[0].cells:
                cell.width = Cm(6.65)
            for cell in keterangan.columns[1].cells:
                cell.width = Cm(0.5)
            for cell in keterangan.columns[2].cells:
                cell.width = Cm(9.25)

            document.add_paragraph()
            closing1 = document.add_paragraph(
                'Sehubungan dengan hal tersebut, dengan ini Kami perintahkan kepada Dana Pensiun Lembaga Keuangan PT Bank Mandiri (Persero) Tbk. untuk membayarkan Manfaat Dana PPUKP kepada Karyawan Kami yang tersebut diatas pada tanggal ' + str(self.formatdate(self.due_date)) + '.\n')
            closing1.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

            closing2 = document.add_paragraph(
                'Demikian Surat permintaan pembayaran Manfaat Dana PPUKP ini kami sampaikan sebagai pengganti Formulir Pembayaran Manfaat Dana PPUKP, untuk ditindaklanjuti dan dijalankan sebagaimana mestinya.')
            closing2.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

            document.add_paragraph()
            document.add_paragraph()
            hormat = document.add_paragraph('Hormat kami,\n')
            hormat.add_run('Petrogas (Basin) Ltd.\n\n\n\n\n').bold = True
            gm = hormat.add_run('Syafri Syafar\n')
            gm.bold = True
            gm.underline = True
            hormat.add_run('General Manager / Country Manager')

            file_with_path = str(os.path.dirname(os.path.realpath(__file__)))[
                :-6] + '/static/doc/' + 'Surat untuk Mandiri PPUKP ' + str(self.employee_id.name) + '.docx'
            if path.exists(file_with_path):
                os.remove(file_with_path)
            document.save(file_with_path)
            return {
                'type': 'ir.actions.act_url',
                'url': 'report_petrogas/static/doc/' + 'Surat untuk Mandiri PPUKP ' + str(self.employee_id.name) + '.docx',
                'target': 'self',
            }


class WizardReportSalaryList(models.TransientModel):
    _name = 'report.salary.list'
    _description = 'Report Excel Petrogas'

    batch_id = fields.Many2one('hr.payslip.run', required=True)
    type_report = fields.Selection([
        ('salary', 'Salary List'),
        ('emp', 'EMP List'),
        ('dplk', 'DPLK'),
        ('pph', 'PPh'),
        ('mcm', 'MCM'),
    ], string='Select Report', required=True)
    location = fields.Selection(
        [('jakarta', 'Jakarta'), ('sorong', 'Sorong')])

    @api.multi
    def action_download(self):
        context = dict(self._context or {})
        type_report = self.type_report
        if type_report == 'mcm':
            return {
                'type': 'ir.actions.act_url',
                'url': '/web/binary/download_mcm?model=hr.payslip.run&id=%s&filename=%s.csv' % (str(self.batch_id.id), self.batch_id.name),
                'target': 'new',
            }
        datas = {}
        datas['model'] = 'report.salary.list'
        datas['form'] = self.read()[0]
        for field in datas['form'].keys():
            if isinstance(datas['form'][field], tuple):
                datas['form'][field] = datas['form'][field][0]
        if type_report == 'salary':
            name = 'SalaryList Petrogas ' + self.batch_id.name
        elif type_report == 'emp':
            name = 'EMP Petrogas ' + self.batch_id.name
        elif type_report == 'dplk':
            name = 'DATA DPLK Petrogas ' + self.batch_id.name
        elif type_report == 'pph':
            name = '(' + str(self.location) + ')1721-i-' + \
                self.batch_id.date_end[:-3]
        datas['form']['name'] = name
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'report_petrogas.%s_Petrogas.xlsx' % (self.type_report),
            'datas': datas,
            'name': name,
        }


class GenerateReportSalaryList(ReportXlsx):

    def emp_data(self, data):
        emp = self.env['hr.payslip.run'].search(
            [('id', '=', data['form']['batch_id'])])
        return emp

    def rule_data(self, data):
        self.env.cr.execute("""
            SELECT
                hpl.salary_rule_id
            FROM
                hr_payslip_line hpl
                JOIN hr_payslip hp ON hp.id = hpl.slip_id
            WHERE
                hp.payslip_run_id = %s
            GROUP BY
                hpl.salary_rule_id
        """ % (data['form']['batch_id'],))
        res = self.env.cr.fetchall()
        rule_ids = [x[0] for x in res if len(x) == 1 and x[0]]
        rule = self.env['hr.salary.rule'].search([('id', 'in', rule_ids)])
        return rule

    def generate_xlsx_report(self, workbook, data, lines):
        emp = self.emp_data(data)
        rule = self.rule_data(data)

        def extra_info(payslip, sheet, emp_no, rule_no):
            emp_id = payslip.employee_id
            marital = 'M' if emp_id.marital \
                in ('married', 'widower', 'divorced') else 'SG'
            child = str(emp_id.child - 1) \
                if emp_id.marital in ('widower', 'divorced') else str(emp_id.child)
            ptkp = marital + child \
                if emp_id.gender == 'male' or emp_id.marital in ('widower', 'divorced') else 'SG'
            if emp_id.hire_date:
                hire_date_df = datetime.strptime(emp_id.hire_date, '%Y-%m-%d')
                lang = self.env.user.lang
                df = self.env['res.lang'].search(
                    [('code', '=', lang)], limit=1).date_format
                hire_date = hire_date_df.strftime(df)
            else:
                hire_date = ''
            if emp_id.birthday:
                birthday_df = datetime.strptime(emp_id.birthday, '%Y-%m-%d')
                lang = self.env.user.lang
                df = self.env['res.lang'].search(
                    [('code', '=', lang)], limit=1).date_format
                birthday = birthday_df.strftime(df)
            else:
                birthday = ''
            sheet.write(1 + emp_no, rule_no, hire_date, table)
            sheet.write(1 + emp_no, rule_no + 1, '', table)
            sheet.write(1 + emp_no, rule_no + 2, ptkp, table)
            sheet.write(1 + emp_no, rule_no + 3, emp_id.npwp, table)
            sheet.write(1 + emp_no, rule_no + 4,
                        emp_id.bank_account_id.bank_name, table)
            sheet.write(1 + emp_no, rule_no + 5,
                        emp_id.bank_account_id.city, table)
            sheet.write(1 + emp_no, rule_no + 6,
                        emp_id.bank_account_id.owner_name, table)
            sheet.write(1 + emp_no, rule_no + 7,
                        emp_id.bank_account_id.acc_number, table)
            sheet.write(1 + emp_no, rule_no + 8,
                        'M' if emp_id.gender == 'male' else 'F', table)
            sheet.write(1 + emp_no, rule_no + 9, birthday, table)
            sheet.write(1 + emp_no, rule_no + 10, emp_id.job_id.name, table)

        # JKT OPAL
        sheet = workbook.add_worksheet('JKT OPAL')
        sheet.set_column('A:A', 10)
        sheet.set_column('B:B', 8)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 12)
        sheet.set_column('E:E', 21)

        header = workbook.add_format(
            {'font_size': 10, 'align': 'vcenter', 'bold': True})
        header.set_font_name('Arial')
        header.set_align('center')

        header_table = workbook.add_format(
            {'font_size': 10, 'bottom': True, 'align': 'vcenter', 'bold': True})
        header_table.set_font_name('Arial Narrow')
        # header_table.set_align('center')

        table = workbook.add_format(
            {'font_size': 10, 'align': 'vcenter', 'bold': False, 'num_format': '#,###'})
        table.set_font_name('Arial Narrow')
        table.set_align('left')

        table_border = workbook.add_format(
            {'font_size': 10, 'align': 'vcenter', 'bold': False, 'right': True})
        table_border.set_font_name('Arial Narrow')
        table_border.set_align('left')

        sheet.write(0, 0, 'SNO', header_table)
        sheet.write(0, 1, 'PAYROLL', header_table)
        sheet.write(0, 2, 'PAYROLL NAME', header_table)
        sheet.write(0, 3, 'WORKING LOC.', header_table)
        sheet.write(0, 4, 'DEPARTMENT', header_table)
        rule_no = 5
        rule_no_mapping = {}
        for x in rule:
            sheet.write(0, rule_no, x.code, header_table)
            rule_no_mapping[x.id] = rule_no
            rule_no = rule_no + 1
        sheet.write(0, rule_no, 'HIRE_DATE', header_table)
        sheet.write(0, rule_no + 1, 'RESIGN_DATE', header_table)
        sheet.write(0, rule_no + 2, 'PTKP', header_table)
        sheet.write(0, rule_no + 3, 'NPWP', header_table)
        sheet.write(0, rule_no + 4, 'BANK_NAME', header_table)
        sheet.write(0, rule_no + 5, 'BANKBRANCH', header_table)
        sheet.write(0, rule_no + 6, 'BANKCUST', header_table)
        sheet.write(0, rule_no + 7, 'BANKACC', header_table)
        sheet.write(0, rule_no + 8, 'SEX', header_table)
        sheet.write(0, rule_no + 9, 'BIRTH_DATE', header_table)
        sheet.write(0, rule_no + 10, 'TITLE', header_table)

        emp_no = 0
        for payslip in emp.slip_ids.filtered(lambda r: r.employee_id.work_location == 'jakarta'):
            sheet.write(1 + emp_no, 0, payslip.number or '', table)
            sheet.write(1 + emp_no, 1,
                        int(payslip.employee_id.identification_id) or '', table)
            sheet.write(1 + emp_no, 2, payslip.employee_id.name or '', table)
            sheet.write(1 + emp_no, 3,
                        payslip.employee_id.work_location and
                        payslip.employee_id.work_location.capitalize() or '', table_border)
            sheet.write(1 + emp_no, 4,
                        payslip.employee_id.department_id.name or '', table)
            for slip in payslip.line_ids:
                col_to_write = rule_no_mapping.get(slip.salary_rule_id.id)
                if col_to_write:
                    sheet.write(
                        1 + emp_no, col_to_write, slip.total, table)
            for rule_id, col_num in rule_no_mapping.iteritems():
                if rule_id not in payslip.line_ids.mapped('salary_rule_id.id'):
                    sheet.write(1 + emp_no, col_num, 0, table)
            extra_info(payslip, sheet, emp_no, rule_no)
            emp_no = emp_no + 1
        sheet.freeze_panes(1, 4)

        # KMT
        sheet = workbook.add_worksheet('KMT')
        sheet.set_column('A:A', 10)
        sheet.set_column('B:B', 8)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 12)
        sheet.set_column('E:E', 21)

        sheet.write(0, 0, 'SNO', header_table)
        sheet.write(0, 1, 'PAYROLL', header_table)
        sheet.write(0, 2, 'PAYROLL NAME', header_table)
        sheet.write(0, 3, 'WORKING LOC.', header_table)
        sheet.write(0, 4, 'DEPARTMENT', header_table)
        rule_no = 5
        rule_no_mapping = {}
        for x in rule:
            sheet.write(0, rule_no, x.code, header_table)
            rule_no_mapping[x.id] = rule_no
            rule_no = rule_no + 1
        sheet.write(0, rule_no, 'HIRE_DATE', header_table)
        sheet.write(0, rule_no + 1, 'RESIGN_DATE', header_table)
        sheet.write(0, rule_no + 2, 'PTKP', header_table)
        sheet.write(0, rule_no + 3, 'NPWP', header_table)
        sheet.write(0, rule_no + 4, 'BANK_NAME', header_table)
        sheet.write(0, rule_no + 5, 'BANKBRANCH', header_table)
        sheet.write(0, rule_no + 6, 'BANKCUST', header_table)
        sheet.write(0, rule_no + 7, 'BANKACC', header_table)
        sheet.write(0, rule_no + 8, 'SEX', header_table)
        sheet.write(0, rule_no + 9, 'BIRTH_DATE', header_table)
        sheet.write(0, rule_no + 10, 'TITLE', header_table)

        emp_no = 0
        for payslip in emp.slip_ids.filtered(lambda r: r.employee_id.work_location == 'sorong'):
            sheet.write(1 + emp_no, 0, payslip.number or '', table)
            sheet.write(1 + emp_no, 1,
                        int(payslip.employee_id.identification_id) or '', table)
            sheet.write(1 + emp_no, 2, payslip.employee_id.name or '', table)
            sheet.write(1 + emp_no, 3,
                        payslip.employee_id.work_location and
                        payslip.employee_id.work_location.capitalize() or '', table_border)
            sheet.write(1 + emp_no, 4,
                        payslip.employee_id.department_id.name or '', table)
            for slip in payslip.line_ids:
                col_to_write = rule_no_mapping.get(slip.salary_rule_id.id)
                if col_to_write:
                    sheet.write(
                        1 + emp_no, col_to_write, slip.total, table)
            for rule_id, col_num in rule_no_mapping.iteritems():
                if rule_id not in payslip.line_ids.mapped('salary_rule_id.id'):
                    sheet.write(1 + emp_no, col_num, 0, table)
            extra_info(payslip, sheet, emp_no, rule_no)
            emp_no = emp_no + 1
        sheet.freeze_panes(1, 4)

        # JKT
        sheet = workbook.add_worksheet('JKT')
        sheet.set_column('A:A', 10)
        sheet.set_column('B:B', 8)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 12)
        sheet.set_column('E:E', 21)

        sheet.write(0, 0, 'SNO', header_table)
        sheet.write(0, 1, 'PAYROLL', header_table)
        sheet.write(0, 2, 'PAYROLL NAME', header_table)
        sheet.write(0, 3, 'WORKING LOC.', header_table)
        sheet.write(0, 4, 'DEPARTMENT', header_table)
        rule_no = 5
        rule_no_mapping = {}
        for x in rule:
            sheet.write(0, rule_no, x.code, header_table)
            rule_no_mapping[x.id] = rule_no
            rule_no = rule_no + 1
        sheet.write(0, rule_no, 'HIRE_DATE', header_table)
        sheet.write(0, rule_no + 1, 'RESIGN_DATE', header_table)
        sheet.write(0, rule_no + 2, 'PTKP', header_table)
        sheet.write(0, rule_no + 3, 'NPWP', header_table)
        sheet.write(0, rule_no + 4, 'BANK_NAME', header_table)
        sheet.write(0, rule_no + 5, 'BANKBRANCH', header_table)
        sheet.write(0, rule_no + 6, 'BANKCUST', header_table)
        sheet.write(0, rule_no + 7, 'BANKACC', header_table)
        sheet.write(0, rule_no + 8, 'SEX', header_table)
        sheet.write(0, rule_no + 9, 'BIRTH_DATE', header_table)
        sheet.write(0, rule_no + 10, 'TITLE', header_table)

        emp_no = 0
        for payslip in emp.run_nonopal_id.slip_ids:
            sheet.write(1 + emp_no, 0, payslip.number or '', table)
            sheet.write(1 + emp_no, 1,
                        int(payslip.employee_id.identification_id) or '', table)
            sheet.write(1 + emp_no, 2, payslip.employee_id.name or '', table)
            sheet.write(1 + emp_no, 3,
                        payslip.employee_id.work_location and
                        payslip.employee_id.work_location.capitalize() or '', table_border)
            sheet.write(1 + emp_no, 4,
                        payslip.employee_id.department_id.name or '', table)
            for slip in payslip.line_ids:
                col_to_write = rule_no_mapping.get(slip.salary_rule_id.id)
                if col_to_write:
                    sheet.write(
                        1 + emp_no, col_to_write, slip.total, table)
            for rule_id, col_num in rule_no_mapping.iteritems():
                if rule_id not in payslip.line_ids.mapped('salary_rule_id.id'):
                    sheet.write(1 + emp_no, col_num, 0, table)
            extra_info(payslip, sheet, emp_no, rule_no)
            emp_no = emp_no + 1
        sheet.freeze_panes(1, 4)

        # All
        sheet = workbook.add_worksheet('All')
        sheet.set_column('A:A', 10)
        sheet.set_column('B:B', 8)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 12)
        sheet.set_column('E:E', 21)

        sheet.write(0, 0, 'SNO', header_table)
        sheet.write(0, 1, 'PAYROLL', header_table)
        sheet.write(0, 2, 'PAYROLL NAME', header_table)
        sheet.write(0, 3, 'WORKING LOC.', header_table)
        sheet.write(0, 4, 'DEPARTMENT', header_table)
        rule_no = 5
        rule_no_mapping = {}
        for x in rule:
            sheet.write(0, rule_no, x.code, header_table)
            rule_no_mapping[x.id] = rule_no
            rule_no += 1
        sheet.write(0, rule_no, 'HIRE_DATE', header_table)
        sheet.write(0, rule_no + 1, 'RESIGN_DATE', header_table)
        sheet.write(0, rule_no + 2, 'PTKP', header_table)
        sheet.write(0, rule_no + 3, 'NPWP', header_table)
        sheet.write(0, rule_no + 4, 'BANK_NAME', header_table)
        sheet.write(0, rule_no + 5, 'BANKBRANCH', header_table)
        sheet.write(0, rule_no + 6, 'BANKCUST', header_table)
        sheet.write(0, rule_no + 7, 'BANKACC', header_table)
        sheet.write(0, rule_no + 8, 'SEX', header_table)
        sheet.write(0, rule_no + 9, 'BIRTH_DATE', header_table)
        sheet.write(0, rule_no + 10, 'TITLE', header_table)

        emp_no = 0
        # All Jakarta
        for payslip in emp.slip_ids.filtered(lambda r: r.employee_id.work_location == 'jakarta'):
            sheet.write(1 + emp_no, 0, payslip.number or '', table)
            sheet.write(1 + emp_no, 1,
                        int(payslip.employee_id.identification_id) or '', table)
            sheet.write(1 + emp_no, 2, payslip.employee_id.name or '', table)
            sheet.write(1 + emp_no, 3,
                        payslip.employee_id.work_location and
                        payslip.employee_id.work_location.capitalize() or '', table_border)
            sheet.write(1 + emp_no, 4,
                        payslip.employee_id.department_id.name or '', table)
            for slip in payslip.line_ids:
                col_to_write = rule_no_mapping.get(slip.salary_rule_id.id)
                if col_to_write:
                    sheet.write(
                        1 + emp_no, col_to_write, slip.total, table)
            for rule_id, col_num in rule_no_mapping.iteritems():
                if rule_id not in payslip.line_ids.mapped('salary_rule_id.id'):
                    sheet.write(1 + emp_no, col_num, 0, table)
            extra_info(payslip, sheet, emp_no, rule_no)
            emp_no = emp_no + 1

        emp_no = emp_no + 2
        # All Sorong
        for payslip in emp.slip_ids.filtered(lambda r: r.employee_id.work_location == 'sorong'):
            sheet.write(1 + emp_no, 0, payslip.number or '', table)
            sheet.write(1 + emp_no, 1,
                        int(payslip.employee_id.identification_id) or '', table)
            sheet.write(1 + emp_no, 2, payslip.employee_id.name or '', table)
            sheet.write(1 + emp_no, 3,
                        payslip.employee_id.work_location and
                        payslip.employee_id.work_location.capitalize() or '', table_border)
            sheet.write(1 + emp_no, 4,
                        payslip.employee_id.department_id.name or '', table)
            for slip in payslip.line_ids:
                col_to_write = rule_no_mapping.get(slip.salary_rule_id.id)
                if col_to_write:
                    sheet.write(
                        1 + emp_no, col_to_write, slip.total, table)
            for rule_id, col_num in rule_no_mapping.iteritems():
                if rule_id not in payslip.line_ids.mapped('salary_rule_id.id'):
                    sheet.write(1 + emp_no, col_num, 0, table)
            extra_info(payslip, sheet, emp_no, rule_no)
            emp_no = emp_no + 1
        sheet.freeze_panes(1, 4)

GenerateReportSalaryList(
    'report.report_petrogas.salary_Petrogas.xlsx', 'report.salary.list')


class WizardReportEMP(ReportXlsx):

    def emp_data(self, data):
        emp = self.env['hr.payslip.run'].search(
            [('id', '=', data['form']['batch_id'])])
        return emp

    def rule_data(self, data):
        self.env.cr.execute("""
            SELECT
                hpl.salary_rule_id
            FROM
                hr_payslip_line hpl
                JOIN hr_payslip hp ON hp.id = hpl.slip_id
            WHERE
                hp.payslip_run_id = %s
            GROUP BY
                hpl.salary_rule_id
        """ % (data['form']['batch_id'],))
        res = self.env.cr.fetchall()
        rule_ids = [x[0] for x in res if len(x) == 1 and x[0]]
        domain = [
            ('id', 'in', rule_ids),
            ('code', 'not in', ('RPL_BASIC', 'ROA_ALLW', 'RAPEL_ROA', 'PERQUISITE',
                                'RPL_PREQUISITE', 'FBTA_ALLW', 'OPAL'))
        ]
        rule = self.env['hr.salary.rule'].search(domain)
        return rule

    def generate_xlsx_report(self, workbook, data, lines):
        emp = self.emp_data(data)
        rule = self.rule_data(data)
        header = workbook.add_format(
            {'font_size': 12, 'align': 'vcenter', 'bold': True})
        header.set_font_name('Arial')

        header_table = workbook.add_format(
            {'font_size': 12, 'font_name': 'Arial', 'align': 'vcenter', 'align': 'center', 'bold': True})
        header_table_left = workbook.add_format(
            {'font_size': 12, 'font_name': 'Arial', 'align': 'vcenter', 'bold': True})

        table = workbook.add_format({'font_size': 12, 'font_name': 'Arial', 'align': 'vcenter', 'bold': False,
                                     'align': 'center', 'num_format': '#,###'})
        table_left = workbook.add_format(
            {'font_size': 12, 'font_name': 'Arial', 'align': 'vcenter', 'bold': False})

        sheet = workbook.add_worksheet('All')
        sheet.set_column('A:A', 6)
        sheet.set_column('B:B', 46)
        sheet.set_column('C:C', 17)
        sheet.set_column('D:D', 72)
        sheet.set_column('E:E', 12)
        sheet.set_column('F:F', 12)
        sheet.set_column('G:G', 12)
        sheet.set_row(3, 33)

        sheet.write(0, 1, 'PETROGAS (BASIN) LTD', header)
        sheet.write(1, 1, 'PAYROLL ', header)
        sheet.write(1, 2, str(emp.name), header)
        sheet.write(4, 0, 'JAKARTA OFFICE', header_table_left)
        sheet.write(3, 0, 'No', header_table)
        sheet.write(3, 1, 'Name', header_table)
        sheet.write(3, 2, 'Empl#', header_table)
        sheet.write(3, 3, 'Title', header_table)
        sheet.write(3, 4, 'Hire Date', header_table)
        sheet.merge_range('F4:G4', 'Period', header_table)
        sheet.write(4, 5, 'Start', header_table)
        sheet.write(4, 6, 'End', header_table)

        rule_no = 7
        rule_no_mapping = {}
        for x in rule:
            sheet.write(3, rule_no, x.name, header_table)
            rule_no_mapping[x.id] = rule_no
            rule_no += 1

        emp_no = 5
        for payslip in emp.slip_ids.filtered(lambda r: r.employee_id.work_location == 'jakarta'):
            emp_id = payslip.employee_id
            sheet.write(emp_no, 0, emp_no - 4, table)
            sheet.write(emp_no, 1, emp_id.name, table_left)
            sheet.write(emp_no, 2, int(emp_id.identification_id) or '', table)
            sheet.write(emp_no, 3, emp_id.job_id.name or '', table_left)
            sheet.write(emp_no, 4, emp_id.hire_date or '', table)
            sheet.write(emp_no, 5, payslip.date_from or '', table)
            sheet.write(emp_no, 6, payslip.date_to or '', table)
            rule_no = 7
            for slip in payslip.line_ids:
                col_to_write = rule_no_mapping.get(slip.salary_rule_id.id)
                if col_to_write:
                    sheet.write(emp_no, col_to_write, slip.total, table)
            for rule_id, col_num in rule_no_mapping.iteritems():
                if rule_id not in payslip.line_ids.mapped('salary_rule_id.id'):
                    sheet.write(emp_no, col_num, 0, table)
            emp_no += 1

        emp_no += 1
        sheet.write(emp_no, 0, 'KMT SORONG', header_table_left)
        emp_no += 1
        for payslip in emp.slip_ids.filtered(lambda r: r.employee_id.work_location == 'sorong'):
            emp_id = payslip.employee_id
            sheet.write(emp_no, 0, emp_no - 4, table)
            sheet.write(emp_no, 1, emp_id.name, table_left)
            sheet.write(emp_no, 2, int(emp_id.identification_id) or '', table)
            sheet.write(emp_no, 3, emp_id.job_id.name or '', table_left)
            sheet.write(emp_no, 4, emp_id.hire_date or '', table)
            sheet.write(emp_no, 5, payslip.date_from or '', table)
            sheet.write(emp_no, 6, payslip.date_to or '', table)
            rule_no = 7
            for slip in payslip.line_ids:
                col_to_write = rule_no_mapping.get(slip.salary_rule_id.id)
                if col_to_write:
                    sheet.write(emp_no, col_to_write, slip.total, table)
            for rule_id, col_num in rule_no_mapping.iteritems():
                if rule_id not in payslip.line_ids.mapped('salary_rule_id.id'):
                    sheet.write(emp_no, col_num, 0, table)
            emp_no += 1
        sheet.freeze_panes(5, 3)


WizardReportEMP('report.report_petrogas.emp_Petrogas.xlsx',
                'report.salary.list')


class GenerateReportDPLK(ReportXlsx):

    def emp_data(self, data):
        emp = self.env['hr.payslip.run'].search(
            [('id', '=', data['form']['batch_id'])])
        return emp

    def generate_xlsx_report(self, workbook, data, lines):
        emp = self.emp_data(data)
        sheet = workbook.add_worksheet('dplk')
        sheet.set_column('A:A', 3.33)
        sheet.set_column('B:B', 38)
        sheet.set_column('C:C', 12)
        sheet.set_column('D:D', 9.33)
        sheet.set_column('E:E', 14)
        sheet.set_column('F:F', 12)
        sheet.freeze_panes(1, 1)
        sheet.set_row(0, 56)

        header = workbook.add_format(
            {'font_size': 10, 'align': 'vcenter', 'bold': True})
        header.set_font_name('Arial')
        header.set_align('center')

        header_table = workbook.add_format(
            {'font_size': 10, 'bottom': True, 'right': True, 'left': True, 'top': True, 'align': 'vcenter', 'bold': True})
        header_table.set_font_name('Arial')
        header_table.set_align('center')
        # This is optional when using a solid fill.
        header_table.set_pattern(1)
        header_table.set_bg_color('#C0C0C0')
        header_table.set_text_wrap()

        table = workbook.add_format({'font_size': 10, 'font_name': 'Arial', 'bottom': True, 'right': True,
                                     'left': True, 'top': True, 'align': 'vcenter', 'bold': False, 'num_format': '#,###'})
        table_center = workbook.add_format({'font_size': 10, 'font_name': 'Arial', 'bottom': True, 'right': True,
                                            'left': True, 'top': True, 'align': 'center', 'align': 'vcenter', 'bold': False})
        table_bold = workbook.add_format({'font_size': 10, 'font_name': 'Arial', 'bottom': True, 'right': True,
                                          'left': True, 'top': True, 'align': 'vcenter', 'bold': True, 'num_format': '#,###'})

        sheet.write(0, 0, 'NO', header_table)
        sheet.write(0, 1, 'Nama', header_table)
        sheet.write(0, 2, 'Tanggal Lahir', header_table)
        sheet.write(0, 3, 'Nomor Induk Karyawan (NIK)', header_table)
        sheet.write(0, 4, 'No DPLK*', header_table)
        sheet.write(0, 5, 'Total Iuran ', header_table)

        emp_no = 0
        total = 0
        for payslip in emp.slip_ids:
            sheet.write(1 + emp_no, 0, emp_no + 1, table_center)
            sheet.write(1 + emp_no, 1,
                        payslip.employee_id.name or '', table)
            sheet.write(1 + emp_no, 2,
                        payslip.employee_id.birthday or '', table_center)
            sheet.write(1 + emp_no, 3,
                        int(payslip.employee_id.identification_id) or '', table_center)
            sheet.write(1 + emp_no, 4,
                        int(payslip.employee_id.dplk) or 'New Member', table_center)
            amount = 0
            for line in payslip.line_ids.filtered(lambda r: r.code in ('DPLK_EMPL', 'DPLK_COMP')):
                amount += line.total
            sheet.write(1 + emp_no, 5, amount, table)
            total += amount
            emp_no += 1
        sheet.write(1 + emp_no, 5, total, table_bold)


GenerateReportDPLK(
    'report.report_petrogas.dplk_Petrogas.xlsx', 'report.salary.list')


class GenerateReportPPh(ReportXlsx):

    def emp_data(self, data):
        emp = self.env['hr.payslip.run'].search(
            [('id', '=', data['form']['batch_id'])])
        return emp

    def generate_xlsx_report(self, workbook, data, lines):
        emp = self.emp_data(data)
        sheet = workbook.add_worksheet(data['form']['name'])
        sheet.set_column('E:E', 33.17)

        table = workbook.add_format(
            {'font_size': 10, 'bold': False, 'num_format': '#,###'})
        table.set_font_name('Arial')

        sheet.write(0, 0, 'blnpajak', table)
        sheet.write(0, 1, 'thnpajak', table)
        sheet.write(0, 2, 'pembetulan', table)
        sheet.write(0, 3, 'npwp', table)
        sheet.write(0, 4, 'nama', table)
        sheet.write(0, 5, 'kodepjk', table)
        sheet.write(0, 6, 'bruto', table)
        sheet.write(0, 7, 'pph', table)
        sheet.write(0, 8, 'kodeneg', table)

        date_list = emp.date_end.split('-')
        emp_no = 0
        for payslip in emp.slip_ids:
            if payslip.employee_id.work_location == data['form']['location']:
                line_gross = self.env['hr.payslip.line'].search([
                    ('slip_id', '=', payslip.id),
                    ('code', '=', 'GROSSINC'),
                ], limit=1)
                gross = line_gross.total if line_gross else 0
                line_pph = self.env['hr.payslip.line'].search([
                    ('slip_id', '=', payslip.id),
                    ('code', '=', 'INCOME_TAX'),
                ], limit=1)
                pph = line_pph.total if line_pph else 0

                sheet.write(1 + emp_no, 0, date_list[1], table)
                sheet.write(1 + emp_no, 1, date_list[0], table)
                sheet.write(1 + emp_no, 2, str(0), table)
                sheet.write(1 + emp_no, 3,
                            payslip.employee_id.npwp or '', table)
                sheet.write(1 + emp_no, 4,
                            payslip.employee_id.name or '', table)
                sheet.write(1 + emp_no, 5, '21-100-01', table)
                sheet.write(1 + emp_no, 6, gross, table)
                sheet.write(1 + emp_no, 7, pph, table)
                sheet.write(1 + emp_no, 8, '', table)
                emp_no = emp_no + 1

GenerateReportPPh(
    'report.report_petrogas.pph_Petrogas.xlsx', 'report.salary.list')
