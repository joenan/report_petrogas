from openerp import fields, models, api
from datetime import datetime, timedelta


class IMPaymentType(models.Model):
    _name = 'im.payment.type'
    _description = 'IM Payment Type'

    name = fields.Char(string='Name', size=64, required=True)
    rule_ids = fields.Many2many('hr.salary.rule', string="Rule Payment")
    location = fields.Selection(
        [('jakarta', 'Jakarta'), ('sorong', 'Sorong'), ('both', 'Both')], default='both')
    split = fields.Boolean()


class InheriitSalarySlip(models.Model):
    _inherit = 'hr.payslip'

    def formatdate(self, date):
        tgl = datetime.strptime(date, '%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y')
        return tgl


class ResCompany(models.Model):
    _inherit = "res.company"

    ppukp_amount = fields.Float(string='PPUKP')


class HrConfigSetting(models.Model):
    _inherit = 'hr.config.settings'

    ppukp_amount = fields.Float(
        string='PPUKP', related='company_id.ppukp_amount')
