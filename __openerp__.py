# -*- coding: utf-8 -*-
{
    'name': "Report Petrogas",

    'summary': """Custom Report for Petrogas
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Arkana, Joenan <joenan@arkana.co.id>",
    'website': "https://arkana.co.id",
    'email': "info@arkana.co.id",
    'category': 'Human Resources',
    'version': '8.1',
    'depends': ['hr_payroll_petrogas'],
    # always loaded
    'data': [
        'wizard/wizard.xml',
        'security/ir.model.access.csv',
        'views/im_payment_type.xml',
        'views/salary_slip.xml',
        'data/data.xml',
    ],
	'installable': True,
    'application': True,
}