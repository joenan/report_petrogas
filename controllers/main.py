from openerp import http, fields, _
from openerp.http import request
from openerp.addons.web.controllers.main import serialize_exception, content_disposition
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
import csv
# from openerp.exceptions import ValidationError, UserError


class Binary(http.Controller):

    def from_data(self, fields, rows):
        fp = StringIO()
        writer = csv.writer(fp, delimiter=',', quoting=csv.QUOTE_NONE)

        for field in fields:
            writer.writerow([name for name in field])
            # writer.writerow([name.encode('utf-8') for name in field])

        for row in rows:
            for data in row:
                row = []
                for d in data:
                    if isinstance(d, bytes):
                        try:
                            d = d.encode('utf-8')
                        except UnicodeError:
                            pass

                    # Spreadsheet apps tend to detect formulas on leading =, +
                    # and -
                    if type(d) is str and d.startswith(('=', '-', '+')):
                        d = "'" + d

                    row.append(d)
                writer.writerow(row)

        fp.seek(0)
        data = fp.read()
        fp.close()
        return data

    @http.route('/web/binary/download_mcm', type='http', auth="user")
    @serialize_exception
    def download_mcm(self, model, id, filename=None, **kw):
        batch = request.env[model].browse(eval(id))
        date = batch.date_end
        date_list = date.split('-')
        date_str = date_list[0] + date_list[1] + str(25)
        headers = [
            [
                'P', date_str, '1220082801153',
            ],
        ]
        lines = request.env['hr.payslip.line'].search([
            ('payslip_run_id', '=', eval(id)),
            ('code', '=', 'TAKEHOMEPAY'),
        ], order='employee_id')
        export_data = []
        total = 0
        for line in lines:
            # date_format = fields.Date.from_string(inv.date_invoice)
            # partner_company = inv.partner_id.parent_id if inv.partner_id.parent_id else inv.partner_id
            emp_id = line.employee_id
            bank_name = emp_id.bank_account_id.bank_name
            row_data = [
                [
                    emp_id.bank_account_id.acc_number,
                    emp_id.bank_account_id and emp_id.bank_account_id.owner_name or emp_id.name,
                    '',
                    '',
                    '',
                    'IDR',
                    line.total,
                    '',
                    '',
                    'IBU' if bank_name == 'Bank Mandiri' else 'LBU',
                    emp_id.bank_account_id.bank_bic or emp_id.bank_account_id.bank.bic if bank_name != 'Bank Mandiri' else '',
                    bank_name,
                    'JAKARTA',
                    '',
                    '',
                    '',
                    'N',
                ],
            ]
            total += line.total
            export_data.append(row_data)
        count = len(export_data)
        headers[0].append(str(count))
        headers[0].append(str(total))

        filename = batch.name if not filename else filename
        return request.make_response(self.from_data(headers, export_data),
                                     [('Content-Type', 'text/csv;charset=utf8'),
                                      ('Content-Disposition', content_disposition(filename))])
